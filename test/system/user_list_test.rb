require "application_system_test_case"

class UserListTest < ApplicationSystemTestCase
  test "creating a contact" do
    visit new_user_list_path
    fill_in "Name", with: "teste"
    fill_in "Phone number", with: "11965652454"
    click_on "Create User list"
    assert_content "Novo contato criado com sucesso!"
    assert_equal user_lists_path, current_path
  end

  test "updating a contact" do
    user_list = UserList.create(name: "Teste", phone_number:"11424242423")
    visit edit_user_list_path(user_list)
    fill_in "Name", with: "Nova atualização"
    fill_in "Phone number", with: "11932323232"
    click_on "Update User list"
    assert_content "Contato atualizado com sucesso!"
    assert_equal user_lists_path, current_path
    user_list.reload
    assert_equal "Nova atualização", user_list.name
    assert_equal "11932323232", user_list.phone_number
  end

  test "deleting a contact" do
    a = UserList.create(name: "Teste", phone_number:"11424242423")
    visit user_lists_path
    accept_confirm do
      within "#user_list_#{a.id}" do
        click_on "Delete User list"
      end
    end
    assert_content "Contato deletado com sucesso!"
    assert_nil UserList.find_by(id: a.id)
  end
end