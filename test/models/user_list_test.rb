require "test_helper"

class UserListTest < ActiveSupport::TestCase
  test "invalid when phone number length is shorter than 11" do
    user_list = UserList.create(name: "Teste", phone_number:"1")
    assert_not_empty user_list.errors[:phone_number]
  end

  test "invalid when phone number have a string" do
    user_list = UserList.create(name: "Teste", phone_number:"1142424242a")
    assert_not_empty user_list.errors[:phone_number]
  end

  test "invalid when phone number is greater than 11 numbers" do
    user_list = UserList.create(name: "Teste", phone_number:"1142424242113")
    assert_not_empty user_list.errors[:phone_number]
  end

  test "invalid ddd when the ddd invalid" do
    user_list = UserList.create(name: "Teste", phone_number:"00424242423")
    assert_not_empty user_list.errors[:phone_number]
  end
end
