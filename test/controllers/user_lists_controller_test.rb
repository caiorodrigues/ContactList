require "test_helper"

class UserListsControllerTest < ActionDispatch::IntegrationTest
  test "name blank" do
    assert_no_difference "UserList.count" do
      post user_lists_path, params: {user_list: {phone_number: "11968632461"}}
    end
    assert_equal 422, status
  end

  test "phone blank" do
    assert_no_difference "UserList.count" do
      post user_lists_path, params: {user_list: {name: "Teste"}}
    end
    assert_equal 422, status
  end

  test "phone number is invalid" do
    assert_no_difference "UserList.count" do
      post user_lists_path, params: {user_list: {name: "Teste", phone_number: "1"}}
    end
    assert_equal 422, status
  end
end
