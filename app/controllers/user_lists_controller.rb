class UserListsController < ApplicationController
  def index
    @user_list = UserList.all
  end

  def show
    @user_list = UserList.find(params[:id])
  end

  def new
    @user_list = UserList.new
  end

  def create
    @user_list = UserList.new(user_lists_params)
    if @user_list.save
      flash[:notice] = "Novo contato criado com sucesso!"
      redirect_to user_lists_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @user_list = UserList.find(params[:id])
  end

  def update
    @user_list = UserList.find(params[:id])
    if @user_list.update(user_lists_params)
      flash[:notice] = "Contato atualizado com sucesso!"
      redirect_to user_lists_path
    else
       render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @user_list = UserList.find(params[:id])
    @user_list.destroy
    flash[:notice] = "Contato deletado com sucesso!"

    redirect_to user_lists_path
  end

  private

  def user_lists_params
    params.require(:user_list).permit(:name, :phone_number)
  end
end
