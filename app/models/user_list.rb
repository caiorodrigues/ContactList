class UserList < ApplicationRecord
  validates :name, presence: true
  validates :phone_number, presence: true, length: { minimum: 11, maximum: 11 },
            numericality: { only_integer: true }

  validate :check_ddd

  def check_ddd
    ddd = phone_number[0..1].to_i
    if ddd < 11 || ddd > 98
      errors.add(:phone_number, "DDD inválido!")
    end
  end
end
