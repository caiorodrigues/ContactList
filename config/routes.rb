Rails.application.routes.draw do
  resources :users
  root "user_lists#index"

  resources :user_lists
end
